<?php
		session_start();

	$data['type'] = $_POST['type'];
	foreach($_POST as $k => $v){
		$data[$k] = $v;
	}
	if($data['type']==1)
	{
		include_once("./connectdb.php");
		include_once("./function/class.php");
		$call = new call();
		if (!$call->authen_Online())  
		{
			echo "<script>window.location.href = './login.php';</script>";
			exit;
		}
	}
	if(isset($_POST['search']) and $_POST['search'] == 1){
		unset($_POST['search']);
		foreach($_POST as $k => $v){
			if($v != ''){
				$data_s[$k] =$v;
			}
		}
	}
	$array_show = array(
			"n"				,
			'book_date'		,
			'book_code'		,
			'book_name'		,     
			// 'stm_gold'		,  
			// 'stm_price'		,  
			// 'stm_gold_after',    
			// 'stm_type'		,
			// 'code_list'		,
			'book_gold'		, 
			'book_status'		, 
			   
	);
	$array_desc = array(
			"n"				=> array('ลำดับ','center','0'),
			'book_date'		=> array('วันที่สมัคร','center',''),
			'book_code'		=> array('เลขบัญชี','center',''),
			'book_name'		=> array('ชื่อบัญชี','left',''),
			// 'stm_gold'		=> array('จำนวนทอง','center','4'),
			// 'stm_price'		=> array('จำนวนเงิน','center','2',true),
			// 'stm_gold_after'=> array('ยอดคงเหลือ','center','4'),
			// 'stm_type'		=> array('ประเภททำรายการ','center',''),
			// 'code_list'		=> array('Code รายการ','center',''),
			'book_gold'		=> array('น้ำหนักทอง','center','4'),
			'book_status'		=> array('สถานะบัญชี','center',''),
	);	
	$array_search = array(
			'book_date'		=> array('วันที่สมัคร','DATETO','12-6-6',''),//('','','xs-sm-md')
			'book_id'		=> array('รหัสอ้างอิง','HIDDEN','6-6-5','','LIKE'),
			 'book_code'		=> array('เลขบัญชี','TEXT','6-6-2','','LIKE'),
			 'book_name'		=> array('ชื่อบัญชี','TEXT','6-6-2','','LIKE'),
			// 'stm_gold'		=> array('จำนวนทอง','NUMBER','6-6-2'),
			// 'stm_price'		=> array('จำนวนเงิน','NUMBER','6-6-2'),
			// 'stm_gold_after'=> array('ยอดคงเหลือ','NUMBER','6-6-2'),
			// 'stm_type'		=> array('ประเภททำรายการ','TEXT','6-6-5','','%LIKE%'),
			'btn_search'	=> array('Search','SUBMIT','6-6-4'),
			'book_gold'		=> array('น้ำหนักทอง','NUMBER','6-6-2'),
			'book_status'		=> array('สถานะบัญชี','NUMBER','6-6-2'),
	);
	$arr_order	= array(
			'book_date'	=> 'ASC'
	);
	$arr_btn	= array(
			//'export_excel'	=> array('Excel','btn btn-info','fa fa-file-excel-o','excel'),
			//'add_book'	=> array('เปิดบัญชีอออมทอง','btn btn-success','fa fa-plus')
	);
	$arr_where	= array($array_show,$array_search,$data_s);
	$arr_hav	= array('book_code','book_name','book_sponsor_code');
	//$arr_in['s']= array('stm_date','stm_process','stm_type');

	$g_wh	= src::gen_where($arr_where,$arr_hav,$arr_in);

	$sql	="SELECT '' as n
		,b.book_date
		,b.book_code
		,b.book_name
		,b.book_gold
		,b.book_status
		/*,sum(s.stm_price*s.stm_plus) as stm_price*/
	FROM bookx b
	left join bookx_statementx s on b.book_id=s.book_id
	WHERE 1=1 and b.book_sponsor_code='{$_SESSION['book_id']}'  ".$g_wh['where']." group by b.book_id Having 1=1 ".$g_wh['hav']." ";
	
	// echo $sql;echo '<br>';
	 
	src::box_search($data_s,$array_search);
	src::gen_dtable($array_show,$array_desc,$array_search,$data_s,'./data/book_statement.php',$arr_btn,$arr_order,$sql);
?>
