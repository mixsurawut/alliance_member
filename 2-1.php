<?php
		session_start();

	$data['type'] = $_POST['type'];
	foreach($_POST as $k => $v){
		$data[$k] = $v;
	}
	if($data['type']==1)
	{
		include_once("./connectdb.php");
		include_once("./function/class.php");
		$call = new call();
		if (!$call->authen_Online())  
		{
			echo "<script>window.location.href = './login.php';</script>";
			exit;
		}
	}
	if(isset($_POST['search']) and $_POST['search'] == 1){
		unset($_POST['search']);
		foreach($_POST as $k => $v){
			if($v != ''){
				$data_s[$k] =$v;
			}
		}
	}
	$array_show = array(
			"n"				,
			'stm_date'		,
			'book_code'		,
			'book_name'		,     
			'stm_gold'		,  
			'stm_price'		,  
			'stm_gold_after',    
			'stm_type'		,
			'code_list'		,
			   
	);
	$array_desc = array(
			"n"				=> array('ลำดับ','center','0'),
			'stm_date'		=> array('วันที่ทำรายการ','center',''),
			'book_code'		=> array('เลขบัญชี','center',''),
			'book_name'		=> array('ชื่อบัญชี','left',''),
			'stm_gold'		=> array('จำนวนทอง','center','4'),
			'stm_price'		=> array('จำนวนเงิน','center','2',true),
			'stm_gold_after'=> array('ยอดคงเหลือ','center','4'),
			'stm_type'		=> array('ประเภททำรายการ','center',''),
			'code_list'		=> array('Code รายการ','center',''),
	);	
	$array_search = array(
			'stm_date'		=> array('วันที่ทำรายการ','DATETO','12-6-6',''),//('','','xs-sm-md')
			'book_id'		=> array('รหัสอ้างอิง','HIDDEN','6-6-5','','LIKE'),
			// 'book_code'		=> array('เลขบัญชี','TEXT','6-6-2','','%LIKE%'),
			// 'book_name'		=> array('ชื่อบัญชี','TEXT','6-6-2','','%LIKE%'),
			// 'stm_gold'		=> array('จำนวนทอง','NUMBER','6-6-2'),
			// 'stm_price'		=> array('จำนวนเงิน','NUMBER','6-6-2'),
			// 'stm_gold_after'=> array('ยอดคงเหลือ','NUMBER','6-6-2'),
			'stm_type'		=> array('ประเภททำรายการ','TEXT','6-6-5','','%LIKE%'),
			'btn_search'	=> array('Search','SUBMIT','6-6-4')  
	);
	$arr_order	= array(
			'stm_date'	=> 'ASC'
	);
	$arr_btn	= array(
			//'export_excel'	=> array('Excel','btn btn-info','fa fa-file-excel-o','excel'),
			//'add_book'	=> array('เปิดบัญชีอออมทอง','btn btn-success','fa fa-plus')
	);
	$arr_where	= array($array_show,$array_search,$data_s);
	$arr_hav	= array('book_code','book_name');
	//$arr_in['s']= array('stm_date','stm_process','stm_type');

	$g_wh	= src::gen_where($arr_where,$arr_hav,$arr_in);

	$sql	="SELECT '' as n
		,b.book_code
		,b.book_name
		,s.stm_date,s.stm_process,s.stm_type,s.stm_hash,s.stm_gold,s.stm_gold_after ,s.stm_price
		,s.stm_id
		,concat(SUBSTRING(s.stm_process,1,1),'-',SUBSTRING(s.stm_hash,1,6)) as code_list
		,IF(stm_plus>0,'ฝาก','ถอน') as stm_plus
	FROM bookx_statementx s
	left join bookx b on b.book_id=s.book_id
	WHERE 1=1 and b.cus_id ='{$_SESSION['id']}' ".$g_wh['where']." Having 1=1 ".$g_wh['hav']."";
	src::box_search($data_s,$array_search);
	src::gen_dtable($array_show,$array_desc,$array_search,$data_s,'./data/book_statement.php',$arr_btn,$arr_order,$sql);
?>
