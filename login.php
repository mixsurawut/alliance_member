<!DOCTYPE html>
 <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
<?session_start();?>
        <title>Zarinagroup</title>

        <meta name="description" content="Zarinagroup.">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">
		<link href="./img/web/favicon.png" type="image/x-icon" rel="shortcut icon"> 
        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="./img/favicon.png">
        <link rel="apple-touch-icon" href="./img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="./img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="./img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="./img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="./img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="./img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="./img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="./img/icon180.png" sizes="180x180">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="./css/bootstrap.min.css">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="./css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="./css/main.css">

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="./css/themes.css">
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) -->
        <script src="./js/vendor/modernizr.min.js"></script>
    </head>
    <body>
        <!-- Login Background -->
        <div id="login-background">
            <!-- For best results use an image with a resolution of 2560x400 pixels (prefer a blurred image for smaller file size) -->
            <img src="./img/placeholders/headers/login_header.jpg" alt="Login Background" class="animation-pulseSlow">
        </div>
        <!-- END Login Background -->

        <!-- Login Container -->
        <div id="login-container" class="animation-fadeIn">
            <!-- Login Title -->
            <div class="login-title text-center">
                <h1><i class="gi gi-flash"></i> <strong><br/>ระบบออมทอง<br/> @ by Zarinagroup Co., Ltd.</strong><br><small>Please <strong>Login</strong></small></h1>
            </div>
            <!-- END Login Title -->

            <!-- Login Block -->
            <div class="block push-bit">
                <!-- Login Form -->
                <form method="post" id="form-login" name="form-login" class="form-horizontal form-bordered form-control-borderless">
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="hi hi-user"></i></span>
                                <input type="text" id="user" name="user" class="form-control input-lg" placeholder="User" value="" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                                <input type="password" id="password" value="" name="password" class="form-control input-lg" placeholder="Password" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="input-group" style="width: 100%;" >
						<?php
						  require_once './captcha/securimage.php';
						  $options = array();
						  $options['input_name'] = 'ct_captcha'; // change name of input element for form post

						  if (!empty($_SESSION['ctform']['captcha_error'])) {
							// error html to show in captcha output
							$options['error_html'] = $_SESSION['ctform']['captcha_error'];
						  }

						  echo Securimage::getCaptchaHtml($options);
						?>
                                <!--span class="input-group-addon"><i class="fa fa-exclamation"></i></span>
                                <input type="password" id="password" name="password" class="form-control input-lg" placeholder="รหัสยืนยัน" required-->
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                      
                        <div class="col-xs-12 text-center">
                           <div id="error"></div>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                      
                        <div class="col-xs-8 text-right">
                            <button type="button" id="login" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i>เข้าสู่ระบบ</button>
                        </div>
                    </div>
               
                </form>
                <!-- END Login Form -->

                
                
             
            </div>
            <!-- END Login Block -->

            <!-- Footer -->
            <footer class="text-muted text-center">
                <small><span id="year-copy"></span> &copy; <a href="http://zarinashop.com" target="_blank">@ by zarinagroup</a></small>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Login Container -->

        <!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
        <script src="./js/vendor/jquery.min.js"></script>
        <script src="./js/vendor/bootstrap.min.js"></script>
        <script src="./js/plugins.js"></script>
        <script src="./js/app.js"></script>

        <!-- Load and execute javascript code used only in this page -->
        <script src="./js/pages/login.js"></script>
        <script>
		$(function(){
			// Login.init(); 
			$("#login").click(function(){
				var usern = $("#user").val();
				var passwordn = $("#password").val();
				var captcha_coden = $("#captcha_code").val();
				if(usern==''){return false;}
				if(passwordn==''){return false;}
				if(captcha_coden==''){return false;}
				var url = "checklogin.php";
				var data = {user:usern,password:passwordn,captcha_code:captcha_coden};
				ajaxpost(url,data);
				setTimeout(function(){document.getElementById('captcha_image').src = './captcha/securimage_show.php?' + Math.random(); this.blur(); return false},200);
			});
		});
		</script>
    </body>
</html>