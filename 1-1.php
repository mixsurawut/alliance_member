<?
	session_start();

	$data['type'] = $_POST['type'];
	foreach($_POST as $k => $v){
		$data[$k] = $v;
	}
	if($data['type']==1)
	{
		include_once("./connectdb.php");
		include_once("./function/class.php");
		$call = new call();
		if (!$call->authen_Online())  
		{
			echo "<script>window.location.href = './login.php';</script>";
			exit;
		}
	}
	if(isset($_POST['search']) and $_POST['search'] == 1){
		unset($_POST['search']);
		foreach($_POST as $k => $v){
			if($v != ''){
				$data_s[$k] =$v;
			}
		}
	}
	$dep_status = array(
		"0"=>'ยกเลิก',
		"1"=>'รออนุมัติ',
		"2"=>'อนุมัติ',
	);
	
	
	if($_SESSION['branch_idx'] == 19){
		$array_show = array(
				"n"			,
				//"print"		,
				"dep_id"	,
				"dep_date"	,
				"branch"	,
				"dep_pay_by",
				//"book"		, 
				"book_code"	, 
				"book_name"	, 
				//"acc_type"	,
				//"owner"		,
				//"under"		,
				"keytype"	,
				"dep_price"	,
				"service_charge"	,
				"dep_gold"	, 
				//"dep_pic"	,
				"dep_status",
				//"user_key",
		); 
	}else{
		$array_show = array(
				"n"			,
				"print"		,
				"dep_id"	,
				"dep_date"	,
				"branch"	,
				"dep_pay_by",
				//"book"		, 
				"book_code"	, 
				"book_name"	, 
				"acc_type"	,
				"trans_type"	,
				// "owner"		,
				// "under"		,
				// "keytype"	,
				"dep_price"	,
				"service_charge"	,
				"dep_gold"	, 
				"dep_pic"	,
				"dep_status",
				"user_key",
		); 
	
	}
	$array_desc = array( 
			"n"				=> array('ลำดับ','center','0'),
			"print"			=> array('พิมพ์','center',''),
			"dep_id"		=> array('รหัสอ้างอิง','center',''),
			"dep_date"		=> array('วันที่','center',''),
			"branch"		=> array('สาขา','center',''),
			"dep_pay_by"	=> array('วิธีชำระ','center',''),
			//"book"			=> array('บัญชี','left',''),
			"book_code"		=> array('เลขบัญชีออมทอง','left',''),
			"book_name"		=> array('ชื่อบัญชี','left',''),
			"acc_type"		=> array('ประเภทบัญชี','center',''),
			"trans_type"		=> array('ประเภทธุรกรรม','center',''),
			// "owner"			=> array('ทำรายการผ่านตู้','center',''),
			// "under"			=> array('รหัสตู้ที่เปิดบัญชี','center',''),
			"keytype"		=> array('ช่องทาง','center',''), 
			"dep_price"		=> array('ยอดเงิน','right','2',true),
			"service_charge"=> array('ค่าบริการ','right','2',true),
			"dep_gold"		=> array('น้ำหนัก/กรัม','center','4',true),
			"dep_pic"		=> array('สลิป','center',''),
			"dep_status"	=> array('สถานะอนุมัติ','center',''),
			"user_key"		=> array('ผู้ทำรายการ','center',''),
	);	
	
	if($_SESSION['branch_idx'] == 19){
		$array_search = array(
			"dep_date"	=> array('วันที่','DATETO','12-6-4',''),
			"dep_id"	=> array('รหัสอ้างอิง','TEXT','6-6-2','','%LIKE%'),
			"branch"	=> array('สาขา','TEXT','6-6-2','','%LIKE%'),
			"dep_pay_by"=> array('วิธีชำระ','TEXT','6-6-2','','%LIKE%'),
			//"book"		=> array('บัญชี','TEXT','6-6-2','','%LIKE%'),
			"book_code"	=> array('เลขบัญชีออมทอง','TEXT','6-6-2','','%LIKE%'),
			"book_name"	=> array('ชื่อบัญชี','TEXT','6-6-2','','%LIKE%'),
			"dep_price"	=> array('ยอดเงิน','NUMBER','6-6-2'),
			"service_charge"=> array('ค่าบริการ','NUMBER','6-6-2'),
			"dep_gold"	=> array('น้ำหนัก/กรัม','NUMBER','6-6-2'),
			//"acc_type2"	=> array('ประเภทธุรกรรม','DROPDOWN','12-6-2',$acc_type), 
			//"owner"		=> array('ทำรายการผ่านตู้','TEXT','6-6-2','','%LIKE%'), 
			//"under"		=> array('รหัสตู้ที่เปิดบัญชี','TEXT','6-6-2','','%LIKE%'), 
			//"keytype"	=> array('ช่องทาง','DROPDOWN','12-6-2',$keytype), 
			"dep_status2"=> array('สถานะอนุมัติ','DROPDOWN','6-6-2',$dep_status),
			"btn_search"=> array('Search','SUBMIT','6-6-1')  
		);
	}else{
		$array_search = array(
			"dep_date"	=> array('วันที่','DATETO','12-6-4',''),
			// "dep_id"	=> array('รหัสอ้างอิง','TEXT','6-6-2','','%LIKE%'),
			// "branch"	=> array('สาขา','TEXT','6-6-2','','%LIKE%'),
			"dep_pay_by"=> array('วิธีชำระ','TEXT','6-6-4','','%LIKE%'),
			//"book"		=> array('บัญชี','TEXT','6-6-2','','%LIKE%'),
			// "book_code"	=> array('เลขบัญชีออมทอง','TEXT','6-6-2','','%LIKE%'),
			// "book_name"	=> array('ชื่อบัญชี','TEXT','6-6-2','','%LIKE%'),
			"dep_price"	=> array('ยอดเงิน','NUMBER','6-6-4'),
			// "service_charge"=> array('ค่าบริการ','NUMBER','6-6-2'),
			// "dep_gold"	=> array('น้ำหนัก/กรัม','NUMBER','6-6-2'),
			// "acc_type2"	=> array('ประเภทบัญชี','DROPDOWN','12-6-2',$acc_type), 
			"trans_type"=> array('ประเภทธุรกรรม','TEXT','12-6-4','','%LIKE%'), 
			// "owner"		=> array('ทำรายการผ่านตู้','TEXT','6-6-2','','%LIKE%'), 
			// "under"		=> array('รหัสตู้ที่เปิดบัญชี','TEXT','6-6-2','','%LIKE%'), 
			// "keytype"	=> array('ช่องทาง','DROPDOWN','12-6-2',$keytype), 
			"dep_status2"=> array('สถานะอนุมัติ','DROPDOWN','6-6-4',$dep_status),
			"btn_search"=> array('Search','SUBMIT','6-6-4')  
		);
	
	}
	// if($acc[16]){$arr_btn['export_excel'] = array('Excel','btn btn-info','fa fa-file-excel-o','excel');}
	$arr_btn['add_deposit'] = array('ทำรายการออมทองคำรูปพรรณ 96.5%','btn btn-success','fa fa-plus','button');
	$arr_order	= array(
		'dep_date'	=> 'DESC',
	);
	$arr_where	= array($array_show,$array_search,$data_s);
	$arr_hav	= array('branch','book','dep_status2','trans_type','acc_type2','under');
	$arr_in['d']= array('keytype');
	$arr_in['b']= array('book_code','book_name');
	$g_wh		= src::gen_where($arr_where,$arr_hav,$arr_in);
	if($_SESSION['branch_idx'] != 0){$where = " and d.branch_id='".$_SESSION['branch_idx']."' ";}
	$sql ="(SELECT '' as print,d.dep_id,d.dep_date,d.dep_pay_by,d.dep_price,d.dep_gold,d.dep_pic,service_charge
	,(SELECT owner_code from Owner_cabinet where id = d.under) as under
	,(SELECT owner_code from Owner_cabinet where id = d.owner_id) as owner
	,if(d.closet<>'',concat('KIOSK (',(SELECT cab_code from Cabinet where  id=d.closet),' )'),br.branch_name) as branch
	,concat('เลขบัญชี : ',b.book_code,'<br>ชื่อบัญชี : ',b.book_name) as book
	,b.book_code,b.book_name
	,CASE d.keytype
		WHEN 1 THEN 'Line'
		WHEN 2 THEN 'Kiosk'
		WHEN 3 THEN 'Website'
		WHEN 4 THEN 'App'
		WHEN 5 THEN 'Backoffice'
		ELSE ''
	END as keytype
	,IF( d.register = 1,
		'เปิดบัญชีออมทอง',if(d.id_promotion!=0,(SELECT pro_name FROM `promotion` where id =d.id_promotion),'ออมทอง')
	) as trans_type 
	, CASE b.acc_type 
		WHEN 1 THEN 'บัญชีออมทอง'
		WHEN 3 THEN concat('บัญชีฝากประจำ 3 เดือน (',FORMAT(b.price_savings,0),')')
		WHEN 6 THEN concat('บัญชีฝากประจำ 6 เดือน (',FORMAT(b.price_savings,0),')')
		WHEN 12 THEN concat('บัญชีฝากประจำ 12 เดือน (',FORMAT(b.price_savings,0),')')
		WHEN 24 THEN concat('บัญชีฝากประจำ 24 เดือน (',FORMAT(b.price_savings,0),')')
		END  as acc_type
	,b.acc_type as acc_type2
	,IF(d.dep_status_cancel = 1 ,'ยกเลิกรายการ',IF(d.dep_status_approve = 1,'อนุมัติ','รอการอนุมัติ') ) as dep_status
	,IF(d.dep_status_cancel = 1,'0',IF(d.dep_status_approve = 1,'2','1') ) as dep_status2
	,b.price_savings,d.register,if(d.dep_emp_id_approve<>'',(select emp_name from employeex where emp_id=d.dep_emp_id_approve),'') as userapprove,d.dep_status_approve,b.cus_id,d.dep_status_cancel 
	,IF(d.dep_status_cancel=1 and d.dep_emp_id_cancel>0
		,(SELECT emp_name FROM employeex WHERE emp_id=d.dep_emp_id_cancel)
		,IF(d.dep_status_approve=1 and d.dep_emp_id_approve > 0
			,(SELECT emp_name FROM employeex WHERE emp_id=d.dep_emp_id_approve)
			,''
		)
	) as user_key
	FROM depositx_goldx d
	LEFT JOIN bookx b on d.book_id = b.book_id
	LEFT JOIN branchx br on br.branch_id=d.branch_id WHERE 1=1 and b.cus_id='{$_SESSION['id']}' ".$where.$g_wh['where']." Having 1=1 ".$g_wh['hav'].") tb";
	src::box_search($data_s,$array_search);
	src::gen_dtable($array_show,$array_desc,$array_search,$data_s,'./data/deposit_request.php',$arr_btn,$arr_order,$sql);

?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#add_procuct').click(function(){
			$.get( "pro_editadd.php", function( data ) {
				$( "#add" ).html( data );
				$('#modal-editadd').modal('show');
			});
		});
	});
	function product(pcode){
		 // alert(pcode);
		$.get( "showslip.php?pcode="+pcode, function( data ) {
			$( "#add" ).html( data );
			$('#modal-editadd').modal('show');
		});
	}
	function slipimg(pcode){
		$.get( "showslip.php?pcode="+pcode, function( data ) {
			$( "#add" ).html( data );
			$('#modal-editadd').modal('show');
		});
	}
	function prints(id){
		window.open('pdf_4.php?type=1&id='+id, '_blank');
	}
</script> 