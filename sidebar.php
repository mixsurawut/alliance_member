 <?php
	$arr_sidebar = array(
		array( 'h_name'	=> 'ธุรกรรม' ,
					'h_icon'	=> '<i class="fa fa-database sidebar-nav-icon"></i>' ,
					'h_tab'		=> 1 ,
					'h_sub'		=> array(
						
						array(
							'h_sub_name'	=> 'ออมทองรูปพรรณ (96.5%)',
							'h_sub_link'	=> '1-1',
						),
						array(
							'h_sub_name'	=> 'ถอนทองรูปพรรณ (96.5%)',
							'h_sub_link'	=> '1-2',
						),
						array(
							'h_sub_name'	=> 'สั่งผลิต',
							'h_sub_link'	=> '1-3',
						),
						array(
							'h_sub_name'	=> 'ซื้อทองจากบัญขีอออมทอง (96.5%)',
							'h_sub_link'	=> '1-4',
						),
						array(
							'h_sub_name'	=> 'ตัดขายน้ำหนัก',
							'h_sub_link'	=> '1-5',
						),
						array(
							'h_sub_name'	=> 'ซื้อทองรูปพรรณ (96.5%)',
							'h_sub_link'	=> '1-6',
						),
						
					),
		),
		array( 'h_name'	=> 'ประวัติการทำรายการ' ,
					'h_icon'	=> '<i class="fa fa-newspaper-o sidebar-nav-icon"></i>' ,
					'h_tab'		=> 2 ,
					'h_sub'		=> array(
				array(
							'h_sub_name'	=> 'ประวัติเดินบัญชีออมทอง',
							'h_sub_link'	=> '2-1',
						),
				array(
							'h_sub_name'	=> 'พันธมิตร',
							'h_sub_link'	=> '2-2',
						),
			), 
		),
		array( 'h_name'	=> 'คอมมิชชั่น' ,
					'h_icon'	=> '<i class="fa fa-calculator sidebar-nav-icon"></i>' ,
					'h_tab'		=> 3 ,
					'h_sub'		=> array(
				array(
							'h_sub_name'	=> 'คอมมิชชั่น',
							'h_sub_link'	=> '3-2',
						),

			), 
		),		
		// array( 'h_name'	=> 'ทดสอบ' ,
					// 'h_icon'	=> '<i class="fa fa-newspaper-o sidebar-nav-icon"></i>' ,
					// 'h_tab'		=> 2 ,
					// 'h_sub'		=> array(
				// array(
							// 'h_sub_name'	=> 'ทดสอบ',
							// 'h_sub_link'	=> '3-1',
					// ),
			// ), 
		// ), 
		
			
	);
?>
<!-- Modal -->
<div id="modal-user-settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header text-center">
				<h2 class="modal-title"><i class="fa fa-pencil"></i> Settings</h2>
			</div>
			<div class="modal-body">
				<form action="index.html" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" onsubmit="return false;">
					<fieldset>
						<legend>Vital Info</legend>
						<div class="form-group">
							<label class="col-md-4 control-label">Username</label>
							<div class="col-md-8">
								<p class="form-control-static"><?=$_SESSION['user']?></p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Name</label>
							<div class="col-md-8">
								<p class="form-control-static"><?=$_SESSION['name'].' '.$_SESSION['lname']?></p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Mobile</label>
							<div class="col-md-8">
								<p class="form-control-static"><?=$_SESSION['mobile']?></p>
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend>Password Update</legend>
						<div class="form-group">
							<label class="col-md-4 control-label">Old Password</label>
							<div class="col-md-8">
								<input type="password" id="old_pass" name="old_pass" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">New Password</label>
							<div class="col-md-8">
								<input type="password" id="new_pass" name="new_pass" class="form-control" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Confirm New Password</label>
							<div class="col-md-8">
								<input type="password" id="con_new_pas" name="con_new_pas" class="form-control" placeholder="">
							</div>
						</div>
					</fieldset>
					<div class="form-group form-actions">
						<div class="col-xs-12 text-right">
							<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-sm btn-primary">Save Changes</button>
						</div>
					</div>
				</form>
			</div>
		</div> 
	</div>
</div>
<!-- END Modal -->

<div id="sidebar">
	<div id="sidebar-scroll">
		<div class="sidebar-content">

			<a href="index.php?page=home" >
			   <span class="sidebar-nav-mini-hide">
					<a href="index.php?page=home">
						<img src="../img/web/logo.png" alt="avatar">
					</a>
				</span>
			</a>
			<div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
				<div class="sidebar-user-avatar">
					<a href="index.php">
						<img src="../img/placeholders/avatars/avatar2.jpg" alt="avatar">
					</a>
				</div>
				<div class="sidebar-user-name"><?=$_SESSION['name']?></div>
				<div class="sidebar-user-links">
				
					<!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
					<a href="javascript:void(0)" class="enable-tooltip" data-placement="bottom" title="Settings" onclick="$('#modal-user-settings').modal('show');"><i class="gi gi-cogwheel"></i></a>
					<a href="logout" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
				</div>
			</div>
			<ul class="sidebar-nav">
				<?php
					foreach($arr_sidebar as $k => $v){
						
					?>
					
						<li <?if($v['h_tab']==$_GET['headtab']){echo 'class="active"';}?>>
							<a href="#" class="sidebar-nav-menu">
								<i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>
								<?=$v['h_icon']?>
								<span class="sidebar-nav-mini-hide" ><?=$v['h_name']?></span>
							</a>
							<?php if($v['h_sub'] != ''and count($v['h_sub']) > 0){?>
							<ul>
							<?php foreach($v['h_sub'] as $k2 => $v2){ 
							
							?>
							
								<li <?if($v2['h_sub_link']==$_GET['page']){echo 'class="active"';}?>>
									<a href="index.php?headtab=<?=$v['h_tab']?>&page=<?=$v2['h_sub_link']?>" ><?=$v2['h_sub_name']?></a>
								</li>
							<?php }?>
							</ul>
							<?php }?>
						</li>
					<?
					} 
				?>
				<li>&nbsp;</li>
				<li>
					<a href="logout.php" >
						<i class="fa fa-power-off"></i>
						<span class="sidebar-nav-mini-hide">ออกจากระบบ</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>
