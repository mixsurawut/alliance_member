<!DOCTYPE html>
<?
	session_start();
	include_once("./connectdb.php");
	// echo '<pre>';print_r($_SESSION);echo'</pre>';exit;
?>
<html class="no-js" lang="en"> <!--<![endif]-->
<?php require('header.php');?>    
    <body>
        <div id="page-wrapper">
            <div class="preloader themed-background">
                <h1 class="push-top-bottom text-light text-center"><strong>Pro</strong>UI</h1>
                <div class="inner">
                    <h3 class="text-light visible-lt-ie10"><strong>Loading..</strong></h3>
                    <div class="preloader-spinner hidden-lt-ie10"></div>
                </div>
            </div>
			  <div id="add"></div>
			<div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">
				<?php require("sidebar.php");?>
				<div id="main-container">
					<header class="navbar navbar-inverse">
						<ul class="nav navbar-nav-custom">
							<li>
								<a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
									<i class="fa fa-bars fa-fw"></i>
								</a>
							</li>
						</ul>
					</header>
					 
					<?php require("container.php");?>
				</div>
			</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

		<?php require('footer.php');?>
    </body>
</html>