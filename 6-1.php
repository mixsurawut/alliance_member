
<script type="text/javascript">
	$(document).ready(function(){
		$('#edit').click(function(){
			$.get( "edit_cal.php", function( data ) {
				$( "#add" ).html( data );
				$('#modal-editadd').modal('show');
			});
		});
	});
	function edit(i){
		$.get( "edit_cal.php?i="+i, function( data ) {
			$( "#add" ).html( data );
			$('#modal-editadd').modal('show');
		});
	}	
	function update(i,s){
		if(s==0){
			var r = confirm("ต้องการคำนวณการจ่ายรอบ! "+i+" กรุณากดตกลง");
			if (r == true) {
			$("#loading").css("display","block");
			$.post( "../Class/cal.php", { i: i}, function( data ){
				// console.log(data);
				if(data.res==1){
					$("#loading").css("display","none");
					// if(<?=$_SESSION['emp_idx']?>!='48'){
						window.location.replace('index.php?headtab=6&page=6-7&around_id='+i);
					// }
					
				}else{
					alert("ทำรายการไม่สำเร็จกรุณาทำรายการใหม่อีกครั้ัง");
				}
			}, "json");
				
			}
		}else{
			if(s==1){
			alert("ไม่สามารถรทำรายการได้เนื่องจากได้คำนวณรายรอบนี้ไปแล้ว");
			}
			if(s==2){
			alert("ไม่สามารถรทำรายการได้เนื่องจากระบบกำลังประมวลผลอยู่..");
			}
		}
			
	}
	function active(i){
		 var r = confirm("ต้องอนุมัติการจ่ายรอบ Kiosk! "+i+" กรุณากดตกลง");
		if (r == true) {
			$.post( "./Class/around.php",{around:i,chk:'2'}, function( data ) {
				if(data.rul==1){
					location.reload();
				}else{
					alert("ไม่สามารถรทำรายการได้");
				}
			}, "json");
		}
	 }
	function active_m(i){
		 var r = confirm("ต้องอนุมัติการจ่ายรอบ Master! "+i+" กรุณากดตกลง");
		if (r == true) {
			$.post( "./Class/around.php",{around:i,chk:'3'}, function( data ) {
				if(data.rul==1){
					location.reload();
				}else{
					alert("ไม่สามารถรทำรายการได้");
				}
			}, "json");
		}
	 }
	 function viwe(i){
		// index.php?headtab=6&page=6-1
		window.location.replace('index.php?headtab=6&page=6-7&around_id='+i);
	 }
</script>
	<?
	session_start();
	$data['type'] = $_POST['type'];
	foreach($_POST as $k => $v){
		$data[$k] = $v;
	}
	// SELECT *,b.book_code,b.book_name,b.cus_id FROM `withdrawx_goldx` w 
	// left join bookx b on w.book_id=b.book_id
	// where  b.book_status='1' and w.wit_status='1' and b.cus_id!='2844'

	if($data['type']==1)
	{
		include_once("./connectdb.php");
		include_once("./function/class.php");
		$call = new call();
		if (!$call->authen_Online())  
		{
			echo "<script>window.location.href = './login.php';</script>";
			exit;
		}else{
			
		}
	}

	if(isset($_POST['search']) and $_POST['search'] == 1){
		unset($_POST['search']);
		foreach($_POST as $k => $v){
			if($v != ''){
				$data_s[$k] =$v;
			}
		}
	}
	$acc_type = array(
		"1"=>'เปิดบัญชีออมทอง',
		"3"=>'เปิดบัญชีฝากประจำ 3 เดือน',
		"6"=>'เปิดบัญชีฝากประจำ 6 เดือน',
		"12"=>'เปิดบัญชีฝากประจำ 12 เดือน',
		"24"=>'เปิดบัญชีฝากประจำ 24 เดือน',
	);
	$status_cal = array(
		"0"=>'รอการคำนวณ',
		"1"=>'คำนวณแล้ว',
	);
	$status_act = array(
		"0"=>'รอการอนุมัติ',
		"1"=>'อนุมัติแล้ว',
	);
	$array_show = array(
		"n"			,
		"active"	,
		"detail"	,
		"edit"		, 
		"cal"		,
		"around"	, 
		"sdate"		,
		"edate"		,
		"emp_name"	,
		"date_cale"	,
		"status"	,
		
	);
	if(!$acc[2]){
	$key = array_search('active', $array_show);
	unset($array_show[$key]);
	$key = array_search('edit', $array_show);
	unset($array_show[$key]);
	$key = array_search('cal', $array_show);
	unset($array_show[$key]);
	$array_show = array_values($array_show);
	}
	
	$array_desc = array( 
			"n"				=> array('ลำดับ','center','0'),
			"active"		=> array('อนุมัติ','center',''),
			"detail"		=> array('รายงาน','center',''),
			"edit"			=> array('แก้ไข','center',''),
			"around"		=> array('รอบ','center',''),
			"cal"			=> array('คำนวณ','center',''),
			"sdate"			=> array('วันที่เริ่มคำนวณ','center',''),
			"edate"			=> array('วันที่สิ้นสุดคำนวณ','center',''),
			"emp_name"		=> array('ผู้ทำรายการ','left',''),
			"status"		=> array('สถานะคำนวณ','center'),
			"date_cale"		=> array('วันที่คำนวณ','center'),
	);	
	$array_search = array(
			"sdate"			=> array('วันที่เริ่มคำนวณ','DATE','6-6-2'), 
			"edate"			=> array('วันที่สิ้นสุดคำนวณ','DATE','6-6-2'),
			"emp_name"		=> array('ผู้ทำรายการ','TEXT','6-6-2','','%LIKE%'),
			"status"		=> array('สถานะคำนวณ','DROPDOWN','6-6-2',$status_cal),
			"active"		=> array('สถานะอนุมติ','DROPDOWN','6-6-2',$status_act),
			"date_cale"		=> array('วันที่คำนวณ','DATE','6-6-2'), 
			"btn_search"	=> array('Search','SUBMIT','6-6-1')  
	);
	if($acc[1]){$arr_btn['edit'] = array('เพิ่มรอบคำนวณ','btn btn-success','fa fa-plus','button');}
	$arr_order	= array(
		'around'	=> 'DESC',
	);
	$arr_where	= array($array_show,$array_search,$data_s);
	$arr_hav	= array('uid','branch','dep_status');
	$arr_in['e'] = array(
		"emp_name",
	);
	$g_wh = src::gen_where($arr_where,$arr_hav,$arr_in);
	$sql ="(SELECT k.sdate,k.edate,e.emp_name ,k.status,id ,k.around
		,k.active
		,k.active_master
		,concat('<button onclick=\"viwe(',k.id,');\" data-toggle=\"tooltip\"  class=\"btn btn-lg btn-link\" data-original-title=\"Edit\"><i class=\"fa fa-line-chart\"></i> รายละเอียด</button>') as detail
		,concat('<button onclick=\"edit(',k.id,');\" data-toggle=\"tooltip\"  class=\"btn btn-lx btn-warning\" data-original-title=\"Edit\"><i class=\"fa fa-wrench\"></i> แก้ไข</button>') as edit
		,concat('<button onclick=\"update(',k.id,',',k.status,');\" data-toggle=\"tooltip\"  class=\"btn btn-lx btn-success\" data-original-title=\"Edit\"><i class=\"fa fa-arrow-right\"></i>เริ่มคำนวณ</button>') as cal,k.date_cale
			
		FROM around_cal_kiosk k
		left join employeex e on k.uid=e.emp_id
		WHERE 1=1 ".$g_wh['where']." Having 1=1 ".$g_wh['hav']."
		) tb";
	
	src::box_search($data_s,$array_search);
	src::gen_dtable($array_show,$array_desc,$array_search,$data_s,'../Class/set_cal.php',$arr_btn,$arr_order,$sql);
	
?>
<style type="text/css">
	#loading{
		position: fixed;
		width: 100%;
		height: 100%;
		top: 0;
		left: 0;
		background-color: #86868682;
		z-index: 2;
	}
	.loading{
		position: absolute;
		left: 50%;
		top: 50%;
		transform: translate(-50%, -50%);
	}
}
</style>
<div id="loading" style="display:none;">
	<div class="block loading" style="">
		<h3>ระบบกำลังประมวลผลกรุณารอสักครู่ ... </h3> 
		<p><div  class="img"><br/><img src="./img/loading.gif"/></div></p>
	</div>
</div> 